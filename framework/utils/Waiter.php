<?php
namespace framework\utils;
include_once "framework\utils\LoggerUtils.php";

use Facebook\WebDriver\WebDriverExpectedCondition;
use framework\helper\BrowserFactory;

class Waiter
{
    public static function waitForTitleToAppear($title,$timeout = 20)
    {
        LoggerUtils::log(sprintf('Waiting until [%s] appears during [%s] sec', $title, $timeout));
        BrowserFactory::getDriver()->wait()->until(WebDriverExpectedCondition::titleContains($title));
    }
    public static function waitForPageToLoad($timeout = 20) {
        BrowserFactory::getDriver()->wait()->until(function() {
            return BrowserFactory::getDriver()->executeScript('return document.readyState == "complete"');
        });
    }

}