<?php

namespace framework\utils;


class StringUtils
{
    public static function includesWithinArray(string $haystack, array$needles): bool
    {
        foreach ($needles as $needle)
        {
            if (stripos((string)$haystack,$needle) !== false)
                return true;
        }
        return false;
    }

}