<?php

namespace framework\utils;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class LoggerUtils
{
    public static function log(string $message): void
    {
        $logger = self::getLogger();
        $logger->info($message);
    }
    public static function warn(string $message): void
    {
        $logger = self::getLogger();
        $logger->warning($message);
    }
    private static function getLogger()
    {
        $logger = new Logger('logger');
        $logger->pushHandler(new StreamHandler( 'php://stdout',Logger::DEBUG));
        return $logger;
    }

}