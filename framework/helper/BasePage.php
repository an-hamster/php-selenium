<?php
namespace framework\helper;
include_once "framework/elements/Label.php";


use PHPUnit\Framework\Assert;
use framework\elements\Label;

abstract class BasePage
{
    private $pageLocator;
    private $pageName;
    protected $pageUrl;
    protected function __construct($pageLocator,$pageName)
    {
        $this->pageLocator = $pageLocator;
        $this->pageName = $pageName;

        $element = new Label($pageLocator,$pageName);
        Assert::assertTrue($element->isElementPresent(),sprintf('[%s] page was opened', $pageName));

    }



}