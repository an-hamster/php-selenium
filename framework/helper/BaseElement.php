<?php

namespace framework\helper;


use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverExpectedCondition;

abstract class BaseElement
{
    protected $elementLocator;
    protected $elementName;
    protected $elementType;

    protected function __construct($elementLocator, $elementName, $elementType)
    {
        $this->elementLocator = $elementLocator;
        $this->elementName = $elementName;
        $this->elementType = $elementType;
    }

    public function getLocator(): string
    {
        return $this->elementLocator;
    }

    public function getElement(): WebDriverElement
    {
        return BrowserFactory::getDriver()->findElement(WebDriverBy::xpath($this->elementLocator));
    }

    public function isElementPresent(): bool
    {
        $this->waitForElement();
        return $this->getElement()->isDisplayed();
    }

    public function waitForElement(): void
    {
        BrowserFactory::getDriver()->wait()->until(WebDriverExpectedCondition::visibilityOfElementLocated
        (WebDriverBy::xpath($this->elementLocator)));
    }

    public function getElements(): array
    {
        return BrowserFactory::getDriver()->findElements(WebDriverBy::xpath($this->elementLocator));
    }

    public function getText(): array
    {
        return $this->getElement()->getText();
    }

    public function getTextArray(): array
    {
        $elements = $this->getElements();
        $count = 0;
        foreach ($elements as $it)
            $names[$count++] = $it->getText();
        return $names;
    }

    public function isElementEnabled(): bool
    {
        return $this->getElement()->isEnabled();
    }

    public function isElementDisplayed(): bool
    {
        return $this->getElement()->isDisplayed();
    }
    public function click()
    {
        $this->waitForElement();
        $this->getElement()->click();
    }
}