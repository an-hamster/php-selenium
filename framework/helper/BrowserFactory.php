<?php

namespace framework\helper;
include_once "framework\utils\LoggerUtils.php";


use Facebook\WebDriver\JavaScriptExecutor;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use framework\utils\LoggerUtils;
use PHPUnit\Framework\Exception;

class BrowserFactory
{
    private static $instance = null;
    private const HOST = 'http://localhost:4444/wd/hub';
    private static  $driver = null;
    private const PATH = "config.ini";

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::createBrowser();
            self::$instance = new self();
        }
        return self::$instance;
    }
    private static function createBrowser()
    {
        $file = parse_ini_file(self::PATH);
        $browser = $file['browser'];
        switch ($browser) {
            case 'firefox':
                {
                    self::$driver = RemoteWebDriver::create(self::HOST, DesiredCapabilities::firefox());
                    break;
                }
            default :
                {
                    self::$driver = RemoteWebDriver::create(self::HOST, DesiredCapabilities::chrome());
                }
        }
    }



    public function navigateByLink($url): void
    {
        LoggerUtils::log(sprintf("I am going to '%s'",$url));
        self::$driver->navigate()->to($url);
    }
    public static function getDriver()
    {
        self::getInstance();
        return self::$driver;
    }

    public function quit(): void
    {
        LoggerUtils::log("I am closing");
        self::$driver->quit();
    }

    public function refresh(): void
    {
        LoggerUtils::log("Refresh the page");
        self::$driver->navigate()->refresh();
    }


}