<?php
namespace framework\elements;
include_once "framework\helper\BaseElement.php";
use framework\helper\BaseElement;


class TextBox extends BaseElement
{
    protected $elementType = 'TextBox';

    public function __construct($elementLocator, string $elementName)
    {
        parent::__construct($elementLocator, $elementName,$this->elementType);
    }

    public function setText(string $text): void
    {
        $this->getElement()->sendKeys($text);
    }

}