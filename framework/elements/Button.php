<?php
namespace framework\elements;
include_once "framework\helper\BaseElement.php";
use framework\helper\BaseElement;

class Button extends BaseElement
{
    protected $elementType = 'Button';
    public function __construct($elementLocator, $elementName)
    {
        parent::__construct($elementLocator,$elementName,$this->elementType);
    }



}