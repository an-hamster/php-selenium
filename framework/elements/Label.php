<?php

namespace framework\elements;
include_once "framework\helper\BaseElement.php";


use framework\helper\BaseElement;

class Label extends BaseElement
{
    protected $elementType = 'Label';
    public function __construct($elementLocator, $elementName)
    {
        parent::__construct($elementLocator,$elementName,$this->elementType);
    }

}