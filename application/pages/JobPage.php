<?php

namespace application\pages;
include "framework/elements/Button.php";
include "framework/elements/TextBox.php";
include "framework/utils/StringUtils.php";

use framework\elements\Button;
use framework\elements\Label;
use framework\elements\TextBox;
use framework\helper\BasePage;
use framework\utils\LoggerUtils;
use framework\utils\StringUtils;

class JobPage extends BasePage
{
    private $pageName = "Job Page";
    private $txbSearch;
    private $btnFind;
    private $results;
    public function __construct()
    {
        parent::__construct("//span[contains(@class,'jobs-tut-by')]", $this->pageName);
        $this->txbSearch = new TextBox("//input[contains(@class,'VacancyMainSearch')]","Я ищу");
        $this->btnFind = new Button("//span[contains(@class,'search-button')]","Найти");
        $this->results = new Label("//div[contains(@class,'search-item')]//a","Results of Search");
    }

    public function search(string $word)
    {
        $this->txbSearch->setText($word);
        $this->btnFind->click();
    }

    public function assertSearchResults(array $keyWords): bool
    {
        $areCorrect = true;
        $titles = $this->results->getTextArray();
        $index = 1;

        foreach ($titles as $title)
        {
            if (StringUtils::includesWithinArray($title,$keyWords))
            {
                LoggerUtils::log(sprintf("%s '%s' is correct by title",$index,$title));
            }
            else
            {
                LoggerUtils::warn(sprintf("%s '%s' isn't correct by title",$index,$title));
                $areCorrect = false;
            }
            $index++;
        }
        return $areCorrect;
    }




}