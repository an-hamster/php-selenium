<?php
namespace application\pages;
include "framework\helper\BasePage.php";
include "framework\utils\Waiter.php";

use application\menu\MenuForm;
use framework\helper\BasePage;
use framework\utils\Waiter;

class MainPage extends BasePage
{
    private $pageLocator = "//a[@title='TUT.BY - Белорусский портал']";
    public function __construct($pageName = "Home Page")
    {
        parent::__construct($this->pageLocator, $pageName);
    }

    public function getMenu(): MenuForm
    {
        return new MenuForm();
    }


}