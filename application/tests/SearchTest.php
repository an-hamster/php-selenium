<?php
namespace application\tests;
include "framework\helper\BrowserFactory.php";
include "framework\helper\SoftAssert.php";
include "application\pages\MainPage.php";
include "application\pages\JobPage.php";
include "application\menu\MenuForm.php";
include "application/enums/MenuEnum.php";
include_once "framework\utils\LoggerUtils.php";

use application\enums\MenuEnum;
use application\menu\MenuForm;
use application\pages\JobPage;
use framework\helper\BrowserFactory;
use application\pages\MainPage;
use framework\helper\SoftAssert;
use framework\utils\LoggerUtils;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;


class SearchTest extends TestCase
{

   private $testdata = "application/testdata/testdata.ini";
   private $config = "config.ini";

    public function testEnvironment(){

        $file = parse_ini_file($this->testdata);
        $url = parse_ini_file($this->config)['url'];
        BrowserFactory::getInstance()->navigateByLink($url);
        $homePage = new MainPage();
        $homePage->getMenu()->navigate(MenuEnum::JOB);
        $jobPage = new JobPage();
        $jobPage->search($file['search_word']);
        $dictionary = $file['dictionary'];
        $listOfWords = explode(",",$dictionary);
        LoggerUtils::log( sprintf("I search'%s' ",$file['search_word']));
        SoftAssert::assert('assertTrue',$jobPage->assertSearchResults($listOfWords),"All results correspond to search criteria");
        BrowserFactory::getInstance()->quit();

    }

}