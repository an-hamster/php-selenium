<?php

namespace application\enums;


class MenuEnum
{
    public const MINSK = "Минск";
    public const POST = "Почта";
    public const FINANCE = "Финансы";
    public const AFISHA = "Афиша";
    public const JOB = "Работа";
    public const TV = "TV программа";
    public const SHOPS = "Магазины";

}