<?php
namespace application\menu;
include_once "framework\utils\LoggerUtils.php";

use application\pages\MainPage;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use framework\elements\Button;
use framework\elements\Label;
use framework\helper\BrowserFactory;
use framework\utils\LoggerUtils;

class MenuForm extends MainPage
{
    private const MENU_LOCATOR  = "//a[text()='%s']";
    private const CLASS_LOCATOR = "//ul[contains(@class,'topbar-i')]";
    private $pageName = "Menu Form";
    public function __construct()
    {
        parent::__construct(self::CLASS_LOCATOR,$this->pageName);
    }
    public function navigate($menu): void
    {
        $btnMenu = new Button(sprintf(self::CLASS_LOCATOR.self::MENU_LOCATOR,$menu),"Menu option");
        LoggerUtils::log(sprintf("I navigate to '%s' ",$menu));
        $btnMenu->click();
    }

}